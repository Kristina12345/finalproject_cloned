package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import datastruct.Song;

public class Database {
	
	public static Connection conn = null;

	public Database () throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/finalproject", "root", "");
		this.conn.setAutoCommit(false);
	}

	public void setEntries(List<Song> songs) throws Exception {

		String sqlInsert = "insert into finalproject ( ID, Song, Artist, Genre, ReleaseYear) values (?,?,?,?,?)";
		String sqlUpdate = "update finalproject set Song = ?, Artist = ?, Genre = ?, ReleaseYear =? where id = ?";

		PreparedStatement statement = this.conn.prepareStatement(sqlInsert);
		PreparedStatement statementU = this.conn.prepareStatement(sqlUpdate);

		Iterator<Song> iterator = songs.iterator();

		while (iterator.hasNext()) {

			Song currentSong = iterator.next();
			ResultSet rs = conn.createStatement()
					.executeQuery("SELECT * FROM finalproject WHERE id = \"" + currentSong.getID() + "\";");
			if (currentSong.isDelete() == false) {

				if (rs.next()) {
					statementU.setInt(5, currentSong.getID());
					statementU.setString(1, currentSong.getName());
					statementU.setString(2, currentSong.getArtist());
					statementU.setString(3, currentSong.getGenre());
					statementU.setInt(4, currentSong.getReleaseYear());
					statementU.addBatch();
				} else {
					statement.setInt(1, currentSong.getID());
					statement.setString(2, currentSong.getName());
					statement.setString(3, currentSong.getArtist());
					statement.setString(4, currentSong.getGenre());
					statement.setInt(5, currentSong.getReleaseYear());
					statement.addBatch();
				}
			}

		}

		int[] count = statement.executeBatch();
		int[] countU = statementU.executeBatch();

		boolean success = false;
		boolean successU = false;

		for (int countItem : count) {
			if (!(countItem  > 0)) 
				System.out.println("Some entries are not inserted");
			else 
	           success = true;
		}

		for (int countItemU : countU) {
			if (!(countItemU > 0))
				System.out.println("Some entries are not updated");
			else 
	           successU = true;
		}
		
		if (success || successU) {
			this.conn.commit();
			System.out.println("Entries are updated");
		}

	}

}