package task2;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import database.Database;
import datastruct.Song;

public class Search {

	public static void recieveData() {

		ArrayList<Song> songslist = new ArrayList<Song>();
		try {
			Statement statement = Database.conn.createStatement();
			ResultSet srs = statement.executeQuery(Update.strSelect);
			while (srs.next()) {
				Song songs = new Song();
				songs.setID(srs.getInt("ID"));
				songs.setName(srs.getString("song"));
				songs.setArtist(srs.getString("artist"));
				songs.setGenre(srs.getString("genre"));
				songs.setReleaseYear(srs.getInt("releaseYear"));
				songslist.add(songs);
			}

			System.out.println("Total number of records: " + songslist.size());
			for (int i = 0; i < songslist.size(); i++) {
				System.out.println(
						songslist.get(i).getID() + " " + songslist.get(i).getName() + " " + songslist.get(i).getArtist()
								+ " " + songslist.get(i).getGenre() + " " + songslist.get(i).getReleaseYear());
			}

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}
}