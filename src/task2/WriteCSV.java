package task2;

import java.io.*;
import java.sql.*;

import database.Database;

public class WriteCSV {
	public static void WriteCSV() throws Exception {

		Statement statement = Database.conn.createStatement();

		try (ResultSet rs = statement.executeQuery(Update.strSelect)) {

			BufferedWriter fileWriter = new BufferedWriter(
					new FileWriter("C:\\Users\\admin\\Desktop\\Files for the project\\writeFile.csv"));
			fileWriter.write("ID,Song,Artist,Genre,ReleaseYear");

			while (rs.next()) {
				int ID = rs.getInt("ID");
				String name = rs.getString("Song");
				String artist = rs.getString("Artist");
				String genre = rs.getString("Genre");
				int releaseYear = rs.getInt("ReleaseYear");

				String line = String.format("%d,%s,%s,%s,%d", ID, name, artist, genre, releaseYear);

				fileWriter.newLine();
				fileWriter.write(line);
			}
			statement.close();
			fileWriter.close();
			System.out.println("Written.");

		} catch (SQLException e) {
			System.out.println("Datababse error:");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("File IO error:");
			e.printStackTrace();
		}
	}
}