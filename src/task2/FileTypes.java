package task2;

import java.util.Scanner;

public class FileTypes {
	public static void WriteFileTypes() throws Exception {

		System.out.println("\nWhat file do you want to write?\na) XML\nb) JSON\nc) CSV");
		Scanner scanner = new Scanner(System.in);
		String choice2 = scanner.nextLine();

		if (choice2.equals("a")) {
			WriteXML.WriteXML();
		} else if (choice2.equals("b")) {
			WriteJSON.WriteJSON();
		} else {
			WriteCSV.WriteCSV();
		}
	}

}
