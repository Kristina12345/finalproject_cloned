package task2;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;

import database.Database;

public class WriteJSON {
	public static void WriteJSON() throws Exception {

		JSONObject jsonObject = new JSONObject();
		JSONArray array = new JSONArray();
		Statement statement = Database.conn.createStatement();
		ResultSet rs = statement.executeQuery(Update.strSelect);

		while (rs.next()) {
			Map<String, Object> record = new HashMap<String, Object>();

			record.put("@ID", rs.getInt("ID"));
			record.put("Song", rs.getString("Song"));
			record.put("Artist", rs.getString("Artist"));
			record.put("Genre", rs.getString("Genre"));
			record.put("ReleaseYear", rs.getInt("ReleaseYear"));
			
			TreeMap<String, Object> sorted = new TreeMap<>();
			sorted.putAll(record);
			array.put(sorted);
			jsonObject.put("Songs", array);

		}
		try {
			FileWriter fileWriter = new FileWriter(
					"C:\\Users\\admin\\Desktop\\Files for the project\\writeFile.json");
			fileWriter.write(jsonObject.toString(2));
			statement.close();
			fileWriter.close();
			System.out.println("Written.");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}