package task2;

import java.util.Scanner;

public class Update {

	static String strSelect;

	//public static void main(String[] args) throws Exception {
	public static void main(String[] args) throws Exception {
		int searchID;
		String searchSong;
		String searchArtist;
		String searchGenre;
		int searchReleaseYear;

		System.out.println("What field do you what to search?\na) ID\nb) Song\nc) Artist\nd) Genre\ne) Release Year");
		Scanner scanner = new Scanner(System.in);
		String choice = scanner.nextLine();

		if (choice.equals("a")) {
			System.out.println("What ID number do you want to search?");
			searchID = scanner.nextInt();
			strSelect = "SELECT * FROM finalproject where id = " + searchID;
			Search.recieveData();
			FileTypes.WriteFileTypes();
		} else if (choice.equals("b")) {
			System.out.println("What song do you want to search?");
			searchSong = scanner.nextLine();
			strSelect = "SELECT * FROM finalproject where Song = " + searchSong;
			Search.recieveData();
			FileTypes.WriteFileTypes();
		} else if (choice.equals("c")) {
			System.out.println("What artist do you want to search?");
			searchArtist = scanner.nextLine();
			strSelect = "SELECT * FROM finalproject where Artist= " + searchArtist;
			Search.recieveData();
			FileTypes.WriteFileTypes();
		} else if (choice.equals("d")) {
			System.out.println("What genre do you want to search?");
			searchGenre = scanner.nextLine();
			strSelect = "SELECT * FROM finalproject where Genre= " + searchGenre;
			Search.recieveData();
			FileTypes.WriteFileTypes();
		} else if (choice.equals("e")) {
			System.out.println("Enter the year do you want to search?");
			searchReleaseYear = scanner.nextInt();
			strSelect = "SELECT * FROM finalproject where ReleaseYear = " + searchReleaseYear;
			Search.recieveData();
			FileTypes.WriteFileTypes();
		}
	}
}