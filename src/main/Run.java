package main;


import database.Database;
import filetypes.CSV;
import filetypes.FileType;
import openfile.*;

import openfile.Input;


public class Run {

	public static void main(String[] args) throws Exception {
		Input input = new Input();
		input.process();
		
		FileReader fileReader = new FileReader();
		fileReader.setFile(input.getFilePath());
		
		if(input.getFileExtension() == "csv")
			CSV.setDelimiter(";");
		
		FileType fileType = FileType.getInstance(input.getFileExtension());
		fileType.populateData(fileReader.getContent());
		Database db = new Database();
		db.setEntries(fileType.getManysongs());
	    task2.Update.main(args);
		
	}
}
