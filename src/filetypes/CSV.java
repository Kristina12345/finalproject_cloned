package filetypes;

import java.util.ArrayList;
import datastruct.Song;


public class CSV extends FileType {
	private static String delimiter;

	public static void setDelimiter(String delimiter) {
		CSV.delimiter = delimiter;
	}

	@Override
	public void populateData(String content) throws Exception {

		this.manysongs = new ArrayList<Song>();
		String[] line = content.split(System.lineSeparator());
		for (int i = 1; i < line.length; i++) {
			String[] columnNames = line[0].split(";");

			String[] lineValues = line[i].split(";");
			Song song = new Song();
			this.manysongs.add(song);

			for (String j : columnNames) {
				switch (j) {
				case "ID": {
					song.setID(Integer.valueOf(lineValues[0]));
					break;
				}
				case "Name": {
					song.setName(lineValues[1]);
					break;
				}
				case "Artist": {
					song.setArtist(lineValues[2]);
					break;
				}
				case "Genre": {
					song.setGenre(lineValues[3]);
					break;
				}
				case "ReleaseYear": {
					song.setReleaseYear(Integer.valueOf(lineValues[4]));
					break;
				}
				case "Delete": {
					song.setDelete(Boolean.valueOf(lineValues[5]));
					break;
				}
				}
			}
		}
	}
}
