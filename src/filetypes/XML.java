package filetypes;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import datastruct.Song;

public class XML extends FileType {

	@Override
	public void populateData(String content) throws Exception {
		Document xmlDocument = this.convertToXMLDocument(content);
		this.processXMLDocument(xmlDocument);
	}

	private Document convertToXMLDocument(String xmlString) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlString)));
	}

	private void processXMLDocument(Document xmlDocument) {
		Element songs = xmlDocument.getDocumentElement();
		this.manysongs = new ArrayList<Song>();

		NodeList manysongsElements = songs.getElementsByTagName("Song");
		for (int i = 0; i < manysongsElements.getLength(); i++) {
			Element songElement = (Element) manysongsElements.item(i);
			NodeList songParameters = songElement.getChildNodes();

			Song song = new Song();
			this.manysongs.add(song);

			song.setID(Integer.parseInt(songElement.getAttribute("ID")));
			for (int j = 0; j < songParameters.getLength(); j++) {

				Node parameter = songParameters.item(j);

				switch (parameter.getNodeName()) {
				case "ID": {

					song.setID(Integer.parseInt(parameter.getTextContent()));
					break;
				}
				case "Name": {
					song.setName(parameter.getTextContent());
					break;
				}
				case "Artist": {
					song.setArtist(parameter.getTextContent());
					break;
				}
				case "Genre": {
					song.setGenre(parameter.getTextContent());
					break;
				}
				case "ReleaseYear": {
					song.setReleaseYear(Integer.parseInt(parameter.getTextContent()));
					break;
				}
				case "Delete": {
					song.setDelete(Boolean.valueOf(parameter.getTextContent()));
					break;
				}
				}
			}
		}
	}
}