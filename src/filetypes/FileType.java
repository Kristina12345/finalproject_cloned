package filetypes;

import java.util.List;
import datastruct.*;

public abstract class FileType {

	protected List<Song> manysongs;

	public abstract void populateData(String content) throws Exception;

	public List<Song> getManysongs() {
		return this.manysongs;
	}

	public static FileType getInstance(String fileType) {

		switch (fileType) {
		case "csv":
			return new CSV();
		case "json":
			return new JSON();
		case "xml":
			return new XML();
		default:
			return null;
		}
	}

}