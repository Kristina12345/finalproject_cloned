package filetypes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import datastruct.Song;

public class JSON extends FileType {

	@Override
	public void populateData(String content) throws Exception {
		this.manysongs = new ArrayList<Song>();

		JSONObject songsObject = new JSONObject(content);
		JSONArray songsArray = songsObject.getJSONArray("Songs");

		for (int i = 0; i < songsArray.length(); i++) {
			songsObject = songsArray.getJSONObject(i);
			Song songs = new Song();
			this.manysongs.add(songs);

			songs.setID(songsObject.getInt("@ID"));
			songs.setName(songsObject.getString("Name"));
			songs.setArtist(songsObject.getString("Artist"));
			songs.setGenre(songsObject.getString("Genre"));
			songs.setReleaseYear(songsObject.getInt("ReleaseYear"));
			songs.setDelete(songsObject.optBoolean("Delete"));

		}
	}
}