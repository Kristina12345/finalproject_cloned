package datastruct;



public class Song {
	private int ID;
	private String Name;
	private String Artist;
	private String Genre;
	private int ReleaseYear;
	private boolean Delete;
	
	
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getArtist() {
		return Artist;
	}
	public void setArtist(String artist) {
		Artist = artist;
	}
	public int getReleaseYear() {
		return ReleaseYear;
	}
	public void setReleaseYear(int releaseYear) {
		ReleaseYear = releaseYear;
	}
	public boolean isDelete() {
		return Delete;
	}
	public void setDelete(boolean delete) {
		Delete = delete;
	}
	public String getGenre() {
		return Genre;
	}
	public void setGenre(String genre) {
		Genre = genre;
	}

	
}